package com.shirohana.farm_management.util;

public class HttpCustomStatus {
    public static final int RECORD_NOT_FOUND = 600;
    public static final int RECORD_ALREADY_SUBMIT = 601;
    public static final int AUTHENTICATION_ERROR = 602;
}
