package com.shirohana.farm_management.service;

import com.shirohana.farm_management.model.Role;
import com.shirohana.farm_management.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    /**
     * Get all roles
     * @return list of roles
     */
    public List<Role> getAllRoles() {
        try {
            return roleRepository.findAll();
        } catch (Exception e) {
            log.error("Error occurred while fetching all roles: {}", e.getMessage());
            throw new RuntimeException("Failed to fetch roles", e);
        }
    }

    /**
     * Get role by ID
     * @param id role ID
     * @return optional role
     */
    public Optional<Role> getRoleById(Long id) {
        try {
            return roleRepository.findById(id);
        } catch (Exception e) {
            log.error("Error occurred while fetching role with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to fetch role", e);
        }
    }

    /**
     * Create a new role
     * @param role role to create
     * @return created role
     */
    public Role createRole(Role role) {
        try {
            return roleRepository.save(role);
        } catch (Exception e) {
            log.error("Error occurred while creating role: {}", e.getMessage());
            throw new RuntimeException("Failed to create role", e);
        }
    }

    /**
     * Update an existing role
     * @param id role ID
     * @param roleDetails updated role details
     * @return optional updated role
     */
    public Optional<Role> updateRole(Long id, Role roleDetails) {
        try {
            return roleRepository.findById(id).map(role -> {
                role.setName(roleDetails.getName());
                // Update other fields as necessary
                return roleRepository.save(role);
            });
        } catch (Exception e) {
            log.error("Error occurred while updating role with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to update role", e);
        }
    }

    /**
     * Delete a role
     * @param id role ID
     * @return true if deletion was successful, false otherwise
     */
    public boolean deleteRole(Long id) {
        try {
            return roleRepository.findById(id).map(role -> {
                roleRepository.delete(role);
                return true;
            }).orElse(false);
        } catch (Exception e) {
            log.error("Error occurred while deleting role with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Failed to delete role", e);
        }
    }
}
