package com.shirohana.farm_management.service;

import com.shirohana.farm_management.dto.UserDto;
import com.shirohana.farm_management.model.ManagingLevel;
import com.shirohana.farm_management.model.Role;
import com.shirohana.farm_management.model.User;
import com.shirohana.farm_management.repository.UserRepository;
import com.shirohana.farm_management.repository.RoleRepository;
import com.shirohana.farm_management.repository.ManagingLevelRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final ManagingLevelRepository managingLevelRepository;
    private final PasswordEncoder passwordEncoder;

    /**
     * Find all users
     * @return list of users
     */
    public List<User> findAll() {
        try {
            return userRepository.findAll();
        } catch (Exception e) {
            log.error("Error fetching all users: {}", e.getMessage());
            throw new RuntimeException("Error fetching all users", e);
        }
    }

    /**
     * Find user by ID
     * @param id user ID
     * @return optional user
     */
    public Optional<User> findById(Long id) {
        try {
            return userRepository.findById(id);
        } catch (Exception e) {
            log.error("Error fetching user by ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Error fetching user by ID", e);
        }
    }

    /**
     * Find user by string ID (convert to Long)
     * @param id string ID
     * @return optional user
     */
    public Optional<User> findByIdString(String id) {
        try {
            Long longId = Long.parseLong(id);
            return userRepository.findById(longId);
        } catch (NumberFormatException e) {
            log.error("Invalid user ID format: {}", id);
            return Optional.empty();
        }
    }

    /**
     * Create a new user
     * @param userDto user DTO
     * @param createdBy creator user
     * @return created user
     */
    @Transactional
    public User createUser(UserDto userDto, User createdBy) {
        try {
            ManagingLevel userLevel = managingLevelRepository.findByLevel(userDto.getManagingLevel());

            if (createdBy != null && userLevel.getLevel() <= createdBy.getManagingLevel().getLevel()) {
                throw new IllegalArgumentException("You cannot create a user with the same or higher level than yours.");
            }

            User user = User.builder()
                    .username(userDto.getUsername())
                    .password(passwordEncoder.encode(userDto.getPassword()))
                    .enabled(true)
                    .tokenExpired(false)
                    .roles(roleRepository.findByNameIn(userDto.getRoles()))
                    .managingLevel(userLevel)
                    .build();

            return userRepository.save(user);
        } catch (Exception e) {
            log.error("Error creating user: {}", e.getMessage());
            throw new RuntimeException("Error creating user", e);
        }
    }

    /**
     * Update an existing user
     * @param id user ID
     * @param userDto user DTO
     * @param updatedBy updater user
     * @return updated user
     */
    @Transactional
    public Optional<User> updateUser(Long id, UserDto userDto, User updatedBy) {
        try {
            return userRepository.findById(id).map(user -> {
                ManagingLevel userLevel = managingLevelRepository.findByLevel(userDto.getManagingLevel());

                if (updatedBy != null && userLevel.getLevel() <= updatedBy.getManagingLevel().getLevel()) {
                    throw new IllegalArgumentException("You cannot update a user to the same or higher level than yours.");
                }

                user.setUsername(userDto.getUsername());
                if (userDto.getPassword() != null && !userDto.getPassword().isEmpty()) {
                    user.setPassword(passwordEncoder.encode(userDto.getPassword()));
                }
                user.setRoles(roleRepository.findByNameIn(userDto.getRoles()));
                user.setManagingLevel(userLevel);

                return userRepository.save(user);
            });
        } catch (Exception e) {
            log.error("Error updating user with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Error updating user", e);
        }
    }

    /**
     * Delete a user by ID
     * @param id user ID
     * @param deletedBy deleter user
     */
    @Transactional
    public void deleteUser(Long id, User deletedBy) {
        try {
            userRepository.findById(id).ifPresent(user -> {
                if (deletedBy != null && user.getManagingLevel().getLevel() <= deletedBy.getManagingLevel().getLevel()) {
                    throw new IllegalArgumentException("You cannot delete a user with the same or higher level than yours.");
                }
                userRepository.delete(user);
            });
        } catch (Exception e) {
            log.error("Error deleting user with ID {}: {}", id, e.getMessage());
            throw new RuntimeException("Error deleting user", e);
        }
    }

    public boolean isCurrentUserManagingLevelOne(){
        // Get the current authenticated principal (user) from the Security Context
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        // Check if the principal is an instance of the Spring Security User class
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            // Get the username of the authenticated user
            String username = ((org.springframework.security.core.userdetails.User) principal).getUsername();

            // Retrieve the user from the UserRepository using the username
            Optional<User> userOptional = userRepository.findByUsername(username);

            // Check if the user is found
            if (userOptional.isPresent()) {
                // Get the user entity from the Optional
                User user = userOptional.get();

                // Check if the user's managing level is 1
                return user.getManagingLevel().getLevel() == 1;
            }
            // If user is not found, throw a UsernameNotFoundException
            throw new UsernameNotFoundException("User not found: " + username);
        }
        // If the principal is not an instance of the Spring Security User class, return false
        return false;

    }
}
