package com.shirohana.farm_management.repository;

import com.shirohana.farm_management.model.ManagingLevel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ManagingLevelRepository extends JpaRepository<ManagingLevel,Long> {
    ManagingLevel findByLevel(int level);
}
