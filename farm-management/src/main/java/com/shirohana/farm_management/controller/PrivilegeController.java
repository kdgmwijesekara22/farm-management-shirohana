package com.shirohana.farm_management.controller;

import com.shirohana.farm_management.model.Privilege;
import com.shirohana.farm_management.service.PrivilegeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/privileges")
@RequiredArgsConstructor
@Slf4j
public class PrivilegeController {

    private final PrivilegeService privilegeService;

    @GetMapping
    @PreAuthorize("hasAuthority('PRIVILEGE_READ_PRIVILEGES')")
    public ResponseEntity<List<Privilege>> getAllPrivileges() {
        List<Privilege> privileges = privilegeService.getAllPrivileges();
        return ResponseEntity.ok(privileges);
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('PRIVILEGE_READ_PRIVILEGES')")
    public ResponseEntity<Privilege> getPrivilegeById(@PathVariable Long id) {
        Optional<Privilege> privilege = privilegeService.getPrivilegeById(id);
        return privilege.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @PreAuthorize("hasAuthority('PRIVILEGE_CREATE_PRIVILEGE')")
    public ResponseEntity<Privilege> createPrivilege(@RequestBody Privilege privilege) {
        Privilege createdPrivilege = privilegeService.createPrivilege(privilege);
        return ResponseEntity.ok(createdPrivilege);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('PRIVILEGE_UPDATE_PRIVILEGE')")
    public ResponseEntity<Privilege> updatePrivilege(@PathVariable Long id, @RequestBody Privilege privilegeDetails) {
        Optional<Privilege> updatedPrivilege = privilegeService.updatePrivilege(id, privilegeDetails);
        return updatedPrivilege.map(ResponseEntity::ok)
                .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('PRIVILEGE_DELETE_PRIVILEGE')")
    public ResponseEntity<Void> deletePrivilege(@PathVariable Long id) {
        boolean deleted = privilegeService.deletePrivilege(id);
        return deleted ? ResponseEntity.noContent().build() : ResponseEntity.notFound().build();
    }
}
