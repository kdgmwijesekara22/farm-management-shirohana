package com.shirohana.farm_management.controller;

import com.shirohana.farm_management.dto.UserDto;
import com.shirohana.farm_management.model.Role;
import com.shirohana.farm_management.model.User;
import com.shirohana.farm_management.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

import static com.shirohana.farm_management.constants.Permissions.*;

@RestController
@RequestMapping("/api/users")
@RequiredArgsConstructor
@Slf4j
public class UserController {
    private final UserService userService;

    @GetMapping
    @PreAuthorize("hasAuthority('" + USER_READ_PERMISSION + "')") // Check for read permission
    public List<User> getAllUsers() {
        return userService.findAll();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('" + USER_READ_PERMISSION + "')") // Check for read permission
    public ResponseEntity<UserDto> getUserById(@PathVariable Long id) {
        Optional<User> user = userService.findById(id);
        return user.map(u -> {
            UserDto userDto = toUserDto(u);
            return ResponseEntity.ok(userDto);
        }).orElseGet(() -> ResponseEntity.notFound().build());
    }

    @PostMapping
    @PreAuthorize("hasAuthority('" + USER_CREATE_PERMISSION + "')") // Check for create permission
    public ResponseEntity<UserDto> createUser(@RequestBody UserDto userDto) {
        User createdBy = getCurrentUser();
        try {
            User newUser = userService.createUser(userDto, createdBy);
            return ResponseEntity.ok(toUserDto(newUser));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasAuthority('" + USER_UPDATE_PERMISSION + "')") // Check for update permission
    public ResponseEntity<UserDto> updateUser(@PathVariable Long id, @RequestBody UserDto userDto) {
        User updatedBy = getCurrentUser();
        try {
            Optional<User> updatedUser = userService.updateUser(id, userDto, updatedBy);
            return updatedUser.map(user -> ResponseEntity.ok(toUserDto(user)))
                    .orElseGet(() -> ResponseEntity.notFound().build());
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().body(null);
        }
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('" + USER_DELETE_PERMISSION + "')") // Check for delete permission
    public ResponseEntity<Void> deleteUser(@PathVariable Long id) {
        User deletedBy = getCurrentUser();
        try {
            userService.deleteUser(id, deletedBy);
            return ResponseEntity.noContent().build();
        } catch (IllegalArgumentException e) {
            return ResponseEntity.badRequest().build();
        }
    }

    @PostMapping("/public")
    public ResponseEntity<UserDto> createPublicUser(@RequestBody UserDto userDto) {
        try {
            User newUser = userService.createUser(userDto, null); // Public user creation doesn't have a creator
            return ResponseEntity.ok(toUserDto(newUser));
        } catch (Exception e) {
            return ResponseEntity.badRequest().build();
        }
    }

    private User getCurrentUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            String userIdStr = ((org.springframework.security.core.userdetails.User) principal).getUsername();
            Long userId = Long.parseLong(userIdStr);
            return userService.findById(userId).orElse(null);
        }
        return null;
    }

    private UserDto toUserDto(User user) {
        return UserDto.builder()
                .username(user.getUsername())
                .roles(user.getRoles().stream().map(Role::getName).toList())
                .managingLevel(user.getManagingLevel().getLevel())
                .build();
    }
}
