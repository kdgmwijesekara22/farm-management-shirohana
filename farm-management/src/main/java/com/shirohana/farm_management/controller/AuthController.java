package com.shirohana.farm_management.controller;

import com.shirohana.farm_management.dto.AuthRequest;
import com.shirohana.farm_management.dto.AuthResponse;
import com.shirohana.farm_management.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<AuthResponse> login(@RequestBody AuthRequest authRequest){
        String token = authService.authenticate(authRequest.getUsername() , authRequest.getPassword());
        return ResponseEntity.ok(new AuthResponse(token));
    }
}
