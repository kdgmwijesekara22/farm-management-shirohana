package com.shirohana.farm_management.model;

import jakarta.persistence.*;
import lombok.*;

import java.util.Collection;
import java.util.HashSet;

@Entity
@Table(name = "managing_level")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ManagingLevel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private int level;

    @OneToMany(mappedBy = "managingLevel")
    private Collection<User> users = new HashSet<>();
}
