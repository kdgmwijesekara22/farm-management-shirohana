package constants;

public class Permissions {

    public static final String USER_CREATE_PERMISSION = "USER_CREATE_PERMISSION";
    public static final String USER_UPDATE_PERMISSION = "USER_UPDATE_PERMISSION";
    public static final String USER_DELETE_PERMISSION = "USER_DELETE_PERMISSION";
    public static final String USER_READ_PERMISSION = "USER_READ_PERMISSION";

}
